//
//  KLMDataManager.swift
//  KLMData
//
//  Created by Syam Sasidharan on 21/4/19.
//  Copyright © 2019 Syam Sasidharan. All rights reserved.
//

import UIKit
import CoreData
import Foundation

enum KLMDataManagerError: Error {
    case unableToLoadData
}


public class KLMDataManager {
    
    public static let shared = KLMDataManager()
    
    let identifier: String  = kBundleIdentifier
    let model: String       = kModelName
    
    private func getModelUrl() -> URL? {
        
        guard let bundleURL = Bundle(for: KLMDataManager.self).url(forResource: self.identifier, withExtension: "bundle") else {
            return nil
        }
        guard let frameworkBundle = Bundle(url: bundleURL) else {  return nil }
        guard let modelURL = frameworkBundle.url(forResource: self.model, withExtension: kModelFormat) else { return nil }
        
        return modelURL
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        
        let messageKitBundle = Bundle(identifier: self.identifier)
        let modelURL = messageKitBundle!.url(forResource: self.model, withExtension: "momd")!
        let managedObjectModel =  NSManagedObjectModel(contentsOf: modelURL)
        
        
        let container = NSPersistentContainer(name: self.model, managedObjectModel: managedObjectModel!)
        container.loadPersistentStores { (storeDescription, error) in
            
            if let err = error{
                fatalError("Loading of store failed:\(err)")
            }
        }
        
        return container
        
    }()
    
    public func fetch() -> [House]? {
        
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<House>(entityName: kHouseEntityName)
        
        do{
            
            let houses = try context.fetch(fetchRequest)
            
            return houses
            
        }catch let fetchErr {
            print("Failed to fetch houses:",fetchErr)
        }
        
        return nil
    }
    
    private func updateSyncStatus() {
        
        let context = persistentContainer.viewContext
        let syncStatus = NSEntityDescription.insertNewObject(forEntityName: kSyncStatusEntityName, into: context) as! SyncStatus
        
        syncStatus.lastModified = Date()
        
        do {
            try context.save()
            print(" Sync Status saved succesfuly")
            
        } catch let error {
            print(" Failed to create Sync Status: \(error.localizedDescription)")
        }
        
    }
    
    public func createHouse(name : String?, desc: String?, locationUrl: String? , videoUrl : String?, imageUrl : String? , notes : String?) {
        
        
        guard let name = name else {
            return
        }
        
        let context = persistentContainer.viewContext
        let house = NSEntityDescription.insertNewObject(forEntityName: kHouseEntityName, into: context) as! House
        
        house.name = name
        house.desc = desc
        house.locationUrl = locationUrl
        house.videoUrl = videoUrl
        house.imageUrl = imageUrl
        house.notes = notes
        house.id = UUID()
        
        do {
            try context.save()
            print(" House saved succesfuly")
            
        } catch let error {
            print(" Failed to create House: \(error.localizedDescription)")
        }
        
    }
    
    public func addToFavorite(houseId : UUID?) {
        
        
    }
    
    public func refreshStore() {
        
        if isSynced() {
            return
        }
        
        
        //check data already saved if yes dont do anything, else insert data . This data has to be fetched from remote server or synced to persistent store during first launch
        createHouse(name: "House 1", desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque imperdiet, eros eget mollis interdum, metus erat varius ipsum, a porta tortor massa non eros. Nam posuere orci vitae ligula dapibus, a porttitor velit venenatis. Nunc nisl turpis, accumsan in condimentum id, mollis nec arcu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam lacinia eleifend lorem. Sed vel dolor at libero fringilla malesuada. Pellentesque hendrerit vel nunc nec faucibus. In efficitur felis ut tellus auctor, a accumsan nulla auctor. Vivamus vulputate lobortis nisi, in tincidunt elit laoreet a. Quisque eget mi quis nisi ornare porttitor ut a quam. Etiam sagittis dapibus dui, id imperdiet enim suscipit ut. Nam congue diam at ante rutrum porta. Vestibulum eget purus vel ante luctus condimentum at vitae tortor. Cras tempor, neque eget cursus viverra, sem odio elementum sapien, ut dapibus metus arcu a nunc. Nulla dapibus volutpat efficitur. Etiam viverra gravida metus, ut dignissim enim lacinia quis.", locationUrl: "https://goo.gl/maps/HbHGbrGLU8U17sow5", videoUrl: "https://www.youtube.com/watch?v=Vj-kZoLcMtk", imageUrl: "house_1.png", notes: nil)
        
        createHouse(name: "House 2", desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque imperdiet, eros eget mollis interdum, metus erat varius ipsum, a porta tortor massa non eros. Nam posuere orci vitae ligula dapibus, a porttitor velit venenatis. Nunc nisl turpis, accumsan in condimentum id, mollis nec arcu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam lacinia eleifend lorem. Sed vel dolor at libero fringilla malesuada. Pellentesque hendrerit vel nunc nec faucibus. In efficitur felis ut tellus auctor, a accumsan nulla auctor. Vivamus vulputate lobortis nisi, in tincidunt elit laoreet a. Quisque eget mi quis nisi ornare porttitor ut a quam. Etiam sagittis dapibus dui, id imperdiet enim suscipit ut. Nam congue diam at ante rutrum porta. Vestibulum eget purus vel ante luctus condimentum at vitae tortor. Cras tempor, neque eget cursus viverra, sem odio elementum sapien, ut dapibus metus arcu a nunc. Nulla dapibus volutpat efficitur. Etiam viverra gravida metus, ut dignissim enim lacinia quis.", locationUrl: "https://goo.gl/maps/z3yFBq5fzV1yjD6Q7", videoUrl: "https://www.youtube.com/watch?v=ncZQ3xjjbGs", imageUrl: "house_2.png", notes: nil)
        
        updateSyncStatus()
    }
    
    private func isSynced() -> Bool {
        
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<SyncStatus>(entityName: kSyncStatusEntityName)
        
        do{
            
            let syncStatusList = try context.fetch(fetchRequest)
            
            if syncStatusList.count > 0 {
                return true
            }
            
            return false
            
        }catch let fetchErr {
            print("Failed to fetch sync status:",fetchErr)
        }
        
        return false
    }
    
    public func save() {
        
        let context = persistentContainer.viewContext
        
        do {
            try context.save()
            print("Context  saved succesfuly")
            
        } catch let error {
            print(" Failed to error : \(error.localizedDescription)")
        }
    }
    
}

