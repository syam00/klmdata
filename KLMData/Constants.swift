//
//  Constants.swift
//  KLMData
//
//  Created by Syam Sasidharan on 23/4/19.
//  Copyright © 2019 Syam Sasidharan. All rights reserved.
//

import Foundation

public let kHouseEntityName = "House"
public let kFavoritesEntityName = "Favorites"
public let kSyncStatusEntityName = "SyncStatus"
public let kModelName = "DataModel"
public let kBundleIdentifier = "com.heygeek.KLMData"
public let kModelFormat = "momd"
public let kDefaultValue = ""
